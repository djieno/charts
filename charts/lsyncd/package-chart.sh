#!/bin/bash

# Author: Gino Eising

# Function to update the Helm chart index
function package_helm_chart() {
  echo "Packaging Helm chart ..."
  helm package .
  mv *.tgz ../../public
  if [[ $? -ne 0 ]]; then
    echo "Error: Failed to package Helm chart."
    exit 1
  fi
  echo "Helm chart packaged and moved successfully."
}

# Main script execution
package_helm_chart

echo "All tasks completed successfully!"
