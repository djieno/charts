# Helm Chart for [simpleNodeTest](https://gitlab.com/djieno/simplenodetest.git) with Exporter-Specific Configurations
This Helm chart facilitates deploying the simpleNodeTest container with exporter-specific configurations stored in ConfigMaps. It also creates Services and ServiceMonitors for each exporter, enabling Prometheus scraping.

**Prerequisites**
A Kubernetes cluster with Helm 3 installed.
Access to a Kubernetes repository containing the mqtt2prometheus image (e.g., ghcr.io/hikhvar/mqtt2prometheus).

**Installation**

# Place the chart directory in your local Helm repository:
```
helm repo add djieno https://helm.djieno.com

```
# Update your local Helm repository cache:
```
helm repo update
```

# Install the chart:
```
helm upgrade --install simplenodetest --values myvalues.yaml djieno/simplenodetest
```

**Service and ServiceMonitor Details**
By default, the chart creates a Service and a ServiceMonitor for the exporter. The Service exposes the exporter on the configured service.port, while the ServiceMonitor defines scraping configuration for Prometheus.

***Service:***

Type: Defined by service.type in values.yaml (defaults to ClusterIP)
Port: Defined by service.port in values.yaml (defaults to 8080)
Target Port: Defined by service.targetPort in values.yaml (defaults to the exporter container port, adjust if different)

***ServiceMonitor:***

Name: Same as the Service name
Scrape Target Port: Defined by service.targetPort in values.yaml
Scrape Interval: Defaults to 15 seconds, adjust in the template if needed.
Scrape Timeout: Defaults to 10 seconds, adjust in the template if needed.
Scrape Path: Assumed to be /metrics, adjust in the template if different.
Important: Ensure your Prometheus instance is configured to discover and scrape these ServiceMonitors.

**Uninstallation**
To uninstall the chart, run:
```
helm uninstall simplenodetest
```
This will remove the deployed resources

**Contributing**
We welcome issue reports and pull requests!

Report issues: Create an issue on GitLab with clear descriptions and steps to reproduce (if possible).
Pull requests: Fork the repo, make changes, create a branch, push to your fork, and submit a pull request.
We appreciate your contributions!

**License:** Apache License 2.0
