# Helm Chart for mqtt2prometheus with Exporter-Specific Configurations
This Helm chart facilitates deploying the mqtt2prometheus container with exporter-specific configurations stored in ConfigMaps. It also creates Services and ServiceMonitors for each exporter, enabling Prometheus scraping. This will make use of the project: https://github.com/hikhvar/mqtt2prometheus which maintains the mqtt2prometheus.

**Prerequisites**
A Kubernetes cluster with Helm 3 installed.
Access to a Kubernetes repository containing the mqtt2prometheus image (e.g., ghcr.io/hikhvar/mqtt2prometheus).

**Installation**

# Place the chart directory in your local Helm repository:
```
helm repo add djieno https://helm.djieno.com

```
# Update your local Helm repositGory cache:
```
helm repo update
```

# Install the chart:
```
helm upgrade --install homekit --values myvalues.yaml djieno/mqtt2prometheus
```
Replace homekit with your desired release name. 

Values Configuration
The chart relies on a values.yaml file to configure various deployment aspects. Check the charts/mqtt2prometheus directory for a working values file.

**Service and ServiceMonitor Details**
By default, the chart creates a Service and a ServiceMonitor for the exporter. The Service exposes the exporter on the configured service.port, while the ServiceMonitor defines scraping configuration for Prometheus.

***Service:***

Name: {{ .Chart.Name }}-{{ .Values.exporters.name }} (e.g., my-mqtt2prometheus-homekit)
Type: Defined by service.type in values.yaml (defaults to ClusterIP)
Port: Defined by service.port in values.yaml (defaults to 9641)
Target Port: Defined by service.targetPort in values.yaml (defaults to the exporter container port, adjust if different)

***ServiceMonitor:***

Name: Same as the Service name
Scrape Target Port: Defined by service.targetPort in values.yaml
Scrape Interval: Defaults to 15 seconds, adjust in the template if needed.
Scrape Timeout: Defaults to 10 seconds, adjust in the template if needed.
Scrape Path: Assumed to be /metrics, adjust in the template if different.
Important: Ensure your Prometheus instance is configured to discover and scrape these ServiceMonitors.

**Uninstallation**
To uninstall the chart, run:
```
helm uninstall homekit
```
This will remove the deployed resources

**Contributing**
We welcome issue reports and pull requests!

Report issues: Create an issue on GitLab with clear descriptions and steps to reproduce (if possible).
Pull requests: Fork the repo, make changes, create a branch, push to your fork, and submit a pull request.
We appreciate your contributions!

**License:** Apache License 2.0
