#!/bin/bash

# Get service name and port from values.yaml
SERVICE_NAME=mqtt2prometheus-{{ .Values.exporters.name }}
PORT={{ .Values.service.port }}  # Assuming port is defined under service key

# Display success message and instructions
echo "**Deployment Successful!**"
echo "Your mqtt2prometheus exporter for '{{ .Values.exporters.name }}' has been deployed."
echo ""
echo "To verify the exporter is running, follow these steps:"

echo "1. Identify the service port (default: 9641):"
echo "   If a custom port was defined in values.yaml, it will be used. Otherwise, use 9641."

echo "2. Port Forwarding (access exporter locally):"
echo "   Run 'kubectl port-forward svc/$SERVICE_NAME $PORT:$PORT'"

echo "3. Verify Exporter Running (http://localhost:$PORT):"
echo "   Open a web browser and navigate to http://localhost:$PORT"
echo "   * If successful, you should see the Prometheus scrape endpoint or relevant exporter response."

echo "**Important Notes:**"
echo " * Port forwarding creates a temporary tunnel. Keep the 'kubectl port-forward' command running for access."
echo " * Double-check the port number and service name if the exporter isn't accessible."
echo " * Consider proper authentication for production environments."
