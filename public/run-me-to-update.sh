#!/bin/bash

# Author: Gino Eising

# Function to display usage instructions
usage() {
  echo "Usage: $0 <chart_directory>"
  echo "Arguments:"
  echo "  chart_directory   Directory containing the Helm chart (required). 'simplenodetest' if ../charts/simplenodetest/ contains the files"
}

# Function to package Helm charts
package_helm_charts() {
  local chart_dir="../charts/$1/"

  if ! helm package "$chart_dir"; then
    echo "Error: Failed to package Helm charts from $chart_dir." >&2
    exit 1
  fi
  echo "Helm charts from $chart_dir packaged successfully."
}

# Function to update the Helm chart index
function update_helm_index() {
  echo "Updating Helm chart index..."
  helm repo index .
  if [[ $? -ne 0 ]]; then
    echo "Error: Failed to update Helm chart index."
    exit 1
  fi
  echo "Helm chart index updated successfully."
}

# Function to generate the HTML documentation
function generate_html_docs() {
  echo "Generating HTML documentation..."
  helm repo-html
  if [[ $? -ne 0 ]]; then
    echo "Error: Failed to generate HTML documentation."
    exit 1
  fi
  echo "HTML documentation generated successfully."
}

# Function to commit and push changes to Git
function commit_and_push_changes() {
  echo "Committing and pushing changes to Git..."
  git add .
  git commit -m "Updating Helm chart (new version)"
  git push origin master  
  if [[ $? -ne 0 ]]; then
    echo "Error: Failed to commit and push changes to Git."
    exit 1
  fi
  echo "Changes committed and pushed successfully."
}

# Main script logic
if [[ $# -ne 1 ]]; then
  usage
  exit 1
fi

chart_dir="$1"


# Execute tasks
package_helm_charts "$chart_dir"
update_helm_index
generate_html_docs
commit_and_push_changes

echo "All tasks completed successfully!"

