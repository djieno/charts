# Welcome to the Multi-Chart Helm Repository
This repository houses multiple Helm charts for various applications. Each chart is organized in its own directory.

How to Use the Charts
1. Install Helm:
Ensure you have Helm installed on your system. You can follow the official Helm documentation for installation instructions: https://helm.sh/docs/intro/install/

2. Use the Chart Repository:
- Add this repository to your Helm chart repository list
- update helm
- install/upgrade the chart

```
helm repo add djieno https://helm.djieno.com
helm repo update

helm upgrade --install my-release --values myvalues.yaml djieno/[chartname]
```
NOTE: These are presented as is. Anyone is free to use them, and make suggestions, but they were created for my own use.


Uninstall:
```
helm uninstall my-release
```

**Contributing**
We welcome issue reports and pull requests!

Report issues: Create an issue on GitLab with clear descriptions and steps to reproduce (if possible).
Pull requests: Fork the repo, make changes, create a branch, push to your fork, and submit a pull request.
We appreciate your contributions!

**License:** Apache License 2.0